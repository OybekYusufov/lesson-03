package main

import (
	"fmt"
)

func main(){
	var kredit_miqdori, foiz, oylik_tolov, oy_soni int=0,0,0,0
	fmt.Scanln(&kredit_miqdori)
	fmt.Scanln( &foiz)
	fmt.Scanln(&oy_soni)
	oylik_tolov=(kredit_miqdori+kredit_miqdori*foiz/100)/oy_soni
	var jadval []content

	for i := 0; i < oy_soni; i++ {
		var a content
		a.oy=i+1
		a.oylik_tolov=oylik_tolov
		a.foiz_qarz=kredit_miqdori*foiz*31/(100*365)
		a.asosiy_qarz=oylik_tolov-a.foiz_qarz
		a.qolgan_qarz=kredit_miqdori-a.asosiy_qarz
		if(a.qolgan_qarz<0 || a.oy==oy_soni){
			a.qolgan_qarz=0
		}
		kredit_miqdori=a.qolgan_qarz
		jadval=append(jadval,a)
	}
	fmt.Println("oy  oylik_tolov  asosiy_qarz  foiz_qarz  qolgan_qarz")
	fmt.Println("____________________________________________________")
	for i := 0; i <oy_soni; i++ {
		b:=jadval[i]
		println(b.oy,"  ", b.oylik_tolov,"    ",b.asosiy_qarz,"   ",b.foiz_qarz,"   ",b.qolgan_qarz)
	}
}

type content struct{
	oy int
	oylik_tolov int
	asosiy_qarz int
	foiz_qarz int
	qolgan_qarz int
}